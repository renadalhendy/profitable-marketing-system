import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';

import 'login.dart';
import 'main.dart';

class Splashscreen extends StatefulWidget {
  const Splashscreen({Key key}) : super(key: key);

  @override
  _SplashscreenState createState() => _SplashscreenState();
}
//the loading screen
class _SplashscreenState extends State<Splashscreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SplashScreen(
          seconds: 2,
          navigateAfterSeconds: MyApp(),
          image: Image.asset('images/jp.png'),
          backgroundColor: (Color(0x0ff79A0C9)),
          photoSize: 200,
        ),
      ),
    );
  }
}
