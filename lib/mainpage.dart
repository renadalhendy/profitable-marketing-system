import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_login1/signup.dart';
import 'login.dart';
import 'signup.dart';
import 'mainpage.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'surveys.dart';
import 'offers.dart';
import 'redeem.dart';
import 'settings.dart';

var us;
var points;

////get username function////
getUsername() async {
  var user = await FirebaseAuth.instance.currentUser;
  FirebaseFirestore.instance
      .collection("users")
      .where("email", isEqualTo: user.email)
      .get()
      .then((value) {
    value.docs.forEach((element) {
      us = element.data()["username"];
      points = element.data()["points"];
    });
  });
}

//// switch each item from the popUpMenu and navigate to the specific page////
void onSelected(BuildContext context, int item) async {
  switch (item) {
    case 1:
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => (Surveys()),
          ));
      break;
    case 2:
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => (Offers()),
          ));
      break;
    case 4:
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => (Redeem()),
          ));
      break;
    case 5:
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => (Settingss()),
          ));
      break;
    case 6:
      await FirebaseAuth.instance.signOut();
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (BuildContext context) => Login()));
  }
}

class mainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    getUsername();
    return Scaffold(
        backgroundColor: (Color(0x0ff79A0C9)),
        appBar: AppBar(
          actions: [
            Theme(
                data: Theme.of(context).copyWith(
                    iconTheme: IconThemeData(color: (Color(0x0ff172449)))),
                child: PopupMenuButton<int>( //pop up menu options
                    onSelected: (item) => onSelected(context, item),
                    itemBuilder: (context) => [
                          PopupMenuItem<int>(
                            value: 0,
                            child: Text("$us"),
                          ),
                          PopupMenuDivider(),
                          PopupMenuItem<int>(
                            value: 1,
                            child: Text('Surveys'),
                          ),
                          PopupMenuItem<int>(
                            value: 2,
                            child: Text('Offers'),
                          ),
                          PopupMenuItem<int>(
                            value: 3,
                            child: Text('Activity'),
                          ),
                          PopupMenuItem<int>(
                            value: 4,
                            child: Text('Redeem Points'),
                          ),
                          PopupMenuItem<int>(
                            value: 5,
                            child: Text('Settings'),
                          ),
                          PopupMenuItem<int>(
                            value: 6,
                            child: Text('Log Out'),
                          ),
                          PopupMenuDivider(),
                          PopupMenuItem<int>(
                            value: 7,
                            child: Text('Contact Us'),
                          ),
                          PopupMenuItem<int>(
                            value: 8,
                            child: Text('Facebook'),
                          ),
                        ]))
          ],
          centerTitle: true,
          title: Row(

            children: [
              Image.asset(
                'images/jp.png',
                fit: BoxFit.contain,
                height: 40,
                alignment: FractionalOffset.centerLeft,
              ),
              Container(
                  padding: const EdgeInsets.only(left: 30.0, right: 0),
                  child: Text('$points Points'))
            ],
          ),
        ),
        body: ListView(children: [
          Container(
              child: Padding(
            padding: EdgeInsets.only(right: 9, left: 9, top: 25, bottom: 5),
            child: Column(
              children: <Widget>[
                ///// Gold Surveys /////
                RaisedButton(
                  ////navigate to surveys page////
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Surveys(),
                        ));
                  },
                  child: Text(
                    'Surveys',
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  padding: EdgeInsets.only(
                      right: 135, left: 135, top: 10, bottom: 10),
                  color: Colors.blueGrey,
                ),

                ListTile(
                    leading: Icon(Icons.launch),
                    title: Text("40 points"),
                    subtitle: Text("20 min"),
                    trailing: Icon(Icons.arrow_forward_outlined),
                    onTap: () {
                      //getData();
                    }),
                Divider(height: 5, color: Colors.white),
                ListTile(
                    leading: Icon(Icons.launch),
                    title: Text("20 points"),
                    subtitle: Text("10 min"),
                    trailing: Icon(Icons.arrow_forward_outlined),
                    onTap: () {}),
                Divider(height: 5, color: Colors.white),
                SizedBox(height: 20),

                ///// Gold Offers /////
                RaisedButton(
                  ////navigate to offers page////
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Offers(),
                        ));
                  },
                  child: Text(
                    'Offers',
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  padding: EdgeInsets.only(
                      right: 140, left: 140, top: 10, bottom: 10),
                  color: Colors.blueGrey,
                ),
                ListTile(
                    leading: Icon(Icons.launch),
                    title: Text("40 points"),
                    subtitle: Text("Purchase Item"),
                    trailing: Icon(Icons.arrow_forward_outlined),
                    onTap: () {}),
                Divider(height: 5, color: Colors.white),
                ListTile(
                    leading: Icon(Icons.launch),
                    title: Text("20 points"),
                    subtitle: Text("Download App"),
                    trailing: Icon(Icons.arrow_forward_outlined),
                    onTap: () {}),
                Divider(height: 5, color: Colors.white),
              ],
            ),
          )),
        ]));
  }
}
