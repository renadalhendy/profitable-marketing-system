import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:test_login1/login.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:test_login1/mainpage.dart';
import 'package:test_login1/Splashscreen.dart';

bool isLogin;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  var user = FirebaseAuth.instance.currentUser;
  if (user == null) {
    isLogin = false;
  } else {
    isLogin = true;
  }
  runApp(Splashscreen());
}
class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home:isLogin == true && FirebaseAuth.
      instance.currentUser.emailVerified==true ?
      mainPage() : Login(),
      debugShowCheckedModeBanner: false,
      color: Colors.blueAccent,);
  }}
