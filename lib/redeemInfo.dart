import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:test_login1/mainpage.dart';
import 'redeem.dart';

User user = FirebaseAuth.instance.currentUser;
var oemail;
var onumber;
var oFbAcc;
var ocard;
var points2;
var points3;

CollectionReference order = FirebaseFirestore.instance.collection('orders');
//add an order to redeem points
Future<void> addorder() {
  return order
      .add({
        'oemail': oemail,
        'onumber': onumber,
        'oFbAcc': oFbAcc,
        'ocard': ocard.toString(),
      })
      .then((value) => print("order added"))
      .catchError((error) => print("failed to add order : $error"));
}
//change current user points after redeem
Future<void> changepoints() async {
  var documentID;
  final user = await FirebaseAuth.instance.currentUser;
  FirebaseFirestore _firestore = FirebaseFirestore.instance;
  CollectionReference usersCollection = _firestore.collection('users');
  var querySnapshots =
      await usersCollection.where('email', isEqualTo: user.email).get();
  for (var snapshot in querySnapshots.docs) {
    documentID = snapshot.id;
  }
  print(documentID);
  return FirebaseFirestore.instance
      .collection('users')
      .doc('$documentID')
      .update({'points': points2});
}

class Info extends StatefulWidget {
  @override
  _Info createState() => _Info();
}

class _Info extends State<Info> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: (Color(0x0ff79A0C9)),
      appBar: AppBar(
          title: Text('User Information'),
          backgroundColor: (Color(0x0ff4676B4))),
      body: ListView(
        children: [
          SizedBox(height: 25),
          Container(
            padding: EdgeInsets.only(right: 40, left: 40, top: 0, bottom: 10),
            child: Form(
              child: Column(
                children: [
                  Center(
                    child: Text('Sumbit A Request',
                        style: TextStyle(
                            color: (Color(0x0ffF6F7F9)), fontSize: 20)),
                  ),
                  SizedBox(height: 45),
                  TextFormField(
                    onChanged: (val) {
                      setState(() {
                        oemail = val;
                      });
                    },
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      labelText: 'Email',
                      labelStyle: TextStyle(color: Colors.blueGrey),
                      errorStyle: TextStyle(color: Colors.red),
                    ),

                  ),
                  SizedBox(height: 30),
                  TextFormField(
                    onChanged: (val) {
                      setState(() {
                        onumber = val;
                      });
                    },
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      labelText: 'Phone Number',
                      labelStyle: TextStyle(color: Colors.blueGrey),
                      errorStyle: TextStyle(color: Colors.red),
                    ),
                  ),
                  SizedBox(height: 30),
                  TextFormField(
                    onChanged: (val) {
                      setState(() {
                        oFbAcc = val;
                      });
                    },
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      labelText: 'Facebook Account',
                      labelStyle: TextStyle(color: Colors.blueGrey),
                      errorStyle: TextStyle(color: Colors.red),
                    ),
                  ),
                  SizedBox(height: 30),
                  RaisedButton(
                    onPressed: () {
                      if (selectedcard <= points && user.email == oemail) {
                        addorder();

                        points2 = points - selectedcard;
                        ocard = selectedcard;
                        changepoints();
                        AwesomeDialog(
                            context: context,
                            title: "Order received",
                            body: Text("we have received your order"))
                          ..show();
                      } else {
                        AwesomeDialog(
                            context: context,
                            title: "Error",
                            body: Text("Something went wrong with your order"))
                          ..show();
                      }
                    },
                    //

                    child: Text(
                      "Submit Requset",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    color: (Color(0x0ff4676B4)),
                    elevation: 10,
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
