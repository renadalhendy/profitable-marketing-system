import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:test_login1/mainpage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'login.dart';

class signUp extends StatefulWidget {
  signUp({Key key}) : super(key: key);

  _signUp createState() => _signUp();
}

//sign up with a new account
class _signUp extends State<signUp> {
  var username, email, password, phoneNumber;
  var points;
  bool _secureText = true;
  GlobalKey<FormState> formstate = new GlobalKey<FormState>();

  //////////// cloud firestore //////////////
  addData() async {
    CollectionReference usersRef =
        FirebaseFirestore.instance.collection("users");
    usersRef.add({
      "username": username,
      "email": email,
      "points": 0,
    });
  }

  signUp() async {
    var formDate = formstate.currentState;
    if (formDate.validate()) {
      formDate.save();
      try {
        UserCredential userCredential =
            await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: email,
          password: password,
        );
        addData();
        if (userCredential.user.emailVerified == false) {
          User user = FirebaseAuth.instance.currentUser;
          await user.sendEmailVerification();
        }
        return userCredential;
      } on FirebaseAuthException catch (e) {
        if (e.code == 'weak-password') {
          AwesomeDialog(
              context: context,
              title: "Error",
              body: Text("Password is to weak"))
            ..show();
        } else if (e.code == 'email-already-in-use') {
          Navigator.of(context).pop();
          AwesomeDialog(
              context: context,
              title: "Error",
              body: Text("The account already exists for that email"))
            ..show();
        }
      } catch (e) {
        print(e);
      }
    } else {
      print("not valid");
    }
  } //signUp

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: (Color(0x0ff79A0C9)),
      appBar: AppBar(
        title: Text('Sign Up'),
        backgroundColor: (Color(0x0ff4676B4)),
      ),
      body: ListView(children: [
        Center(child: myImage()),
        Container(
            padding: EdgeInsets.only(right: 40, left: 40, top: 0, bottom: 10),
            child: Form(
                key: formstate,
                child: Column(children: [
                  Center(
                      child: Text(
                    'Sign Up ',
                    style: TextStyle(color: (Color(0x0ffF6F7F9)), fontSize: 20),
                  )),
                  SizedBox(height: 5 //10,
                      ),
                  TextFormField(
                    onSaved: (val) {
                      username = val;
                    },
                    validator: (val) {
                      if (val.length > 15) {
                        return "UserName can't to be larger than 15 letter";
                      }
                      if (val.length < 3) {
                        return "UserName can't to be less than 3 letter";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.perm_identity_rounded,
                        color: Colors.white,
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      labelText: 'UserName',
                      hintText: 'ex: JackPot App',
                      labelStyle: TextStyle(color: Colors.blueGrey),
                      errorStyle: TextStyle(color: Colors.red),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    onSaved: (val) {
                      password = val;
                    },
                    validator: (val) {
                      if (val.length > 15) {
                        return "password can't to be larger than 20 letter";
                      }
                      if (val.length < 4) {
                        return "password can't to be less than 3 letter";
                      }
                      return null;
                    },
                    obscureText: _secureText,
                    decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.lock,
                          color: Colors.white,
                        ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: 'Password',
                        //hintText: 'ex: ********',
                        labelStyle: TextStyle(color: Colors.blueGrey),
                        errorStyle: TextStyle(color: Colors.red),
                        suffixIcon: IconButton(
                          icon: Icon(Icons.remove_red_eye),
                          onPressed: () {
                            setState(() {
                              _secureText = !_secureText;
                            });
                          },
                        )),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    onSaved: (val) {
                      email = val;
                    },
                    validator: (val) {
                      if (val.length > 30) {
                        return "email can't to be larger than 20 letter";
                      }
                      if (val.length < 10) {
                        return "email can't to be less than 10 letter";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.email,
                        color: Colors.white,
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      labelText: 'Email',
                      //hintText: 'ex: 0912345678',
                      labelStyle: TextStyle(color: Colors.blueGrey),
                      errorStyle: TextStyle(color: Colors.red),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  SizedBox(height: 10 //10,
                      ),
                  RaisedButton(
                    onPressed: () async {
                      userName() {
                        return username;
                      }

                      UserCredential response = await signUp();
                      print("===================");
                      //print(response.user.email);
                      if (response != null &&
                          FirebaseAuth.instance.currentUser.emailVerified ==
                              true) {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) => mainPage()));
                      } else {
                        AlertDialog alert = AlertDialog(
                          title: const Text('Email Verification'),
                          content: SingleChildScrollView(
                            child: ListBody(
                              children: const <Widget>[
                                //Text('This is a demo alert dialog.'),
                                Text('Please Verify Your Account that we send'),
                              ],
                            ),
                          ),
                        );
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return alert;
                            });
                      }
                      print("===================");
                    },
                    child: Text(
                      'Sign Up',
                      style: TextStyle(color: Colors.white, fontSize: 30),
                    ),
                    color: (Color(0x0ff4676B4)),
                    elevation: 50,
                    padding:
                        EdgeInsets.only(right: 85, left: 85, top: 5, bottom: 5),
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                  ),
                ])))
      ]),
    );
    //);
  }
}
