import 'package:flutter/material.dart';
import 'package:test_login1/login.dart';
import 'mainpage.dart';
import 'redeemInfo.dart';
import 'package:test_login1/redeemInfo.dart';
var card1=500;
var card2=1000;
var card3=1200;
var card4=1500;
var card5=2500;
var selectedcard=0;
class Redeem extends StatefulWidget {
  @override
  _Redeem createState() => _Redeem();
}
//choose a gift card
class _Redeem extends State<Redeem> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: (Color(0x0ff79A0C9)),
        appBar: AppBar(
            title: Text('Redeem Points'),
            backgroundColor: (Color(0x0ff4676B4))),
        body: ListView(
          children: [
            SizedBox(height: 15),
            Center(
              child: Text("Pick Your Gift Card",
                  style: TextStyle(color: Colors.white, fontSize: 20)),
            ),
            SizedBox(height: 15),
            Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    leading: Image.asset('images/jp.png'),
                    title: Text('100 SP'),
                    subtitle: Text("$card1 POINTS"),
                    trailing: Icon(Icons.monetization_on_sharp),
                   // selected: card1==selectedcard,
                    onTap: () {
                      setState(() {
                        selectedcard=card1;
                        print(selectedcard);});
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Info(),));
                    },
                  ),
                  SizedBox(height: 10),
                  Divider(height: 10),
                  ListTile(
                    leading: Image.asset('images/jp.png'),
                    title: Text('200 SP'),
                    subtitle: Text('$card2 POINTS'),
                    trailing: Icon(Icons.monetization_on_sharp),
                    onTap: () {
                      setState(() {
                        selectedcard=card2;
                        print(selectedcard);
                      });
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Info(),
                          ));
                    },
                  ),
                  SizedBox(height: 10),
                  Divider(height: 10),
                  ListTile(
                    leading: Image.asset('images/jp.png'),
                    title: Text('300 SP'),
                    subtitle: Text('$card3 POINTS'),
                    trailing: Icon(Icons.monetization_on_sharp),
                    onTap: () {
                      setState(() {
                        selectedcard=card3;
                        print(selectedcard);
                      });
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Info(),
                          ));
                    },
                  ),
                  SizedBox(height: 10),
                  Divider(height: 10),
                  ListTile(
                    leading: Image.asset('images/jp.png'),
                    title: Text('500 SP'),
                    subtitle: Text('$card4 POINTS'),
                    trailing: Icon(Icons.monetization_on_sharp),
                    onTap: () {
                      setState(() {
                        selectedcard=card4;
                        print(selectedcard);
                      });
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Info(),
                          ));
                    },
                  ),
                  SizedBox(height: 10),
                  Divider(height: 10),
                  ListTile(
                    leading:Image.asset('images/jp.png'),
                    title: Text('1000 SP'),
                    subtitle: Text('$card5 POINTS'),
                    trailing: Icon(Icons.monetization_on_sharp),
                    onTap: () {
                      setState(() {
                        selectedcard=card5;
                        print(selectedcard);
                      });
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Info(),
                          ));
                    },
                  ),
                ],
              ),
            )
          ],
        ));
  }
}
