import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'signup.dart';
import 'mainpage.dart';
import 'package:test_login1/password.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}
//log in class
class _LoginState extends State<Login> {
  var userName, email, password, phoneNumber;
  bool remember = false;
  bool _secureText=true;
  GlobalKey<FormState> formstate = new GlobalKey<FormState>();

  logIn() async {
    var formDate = formstate.currentState;
    if (formDate.validate()) {
      formDate.save();
      try {
        UserCredential userCredential = await FirebaseAuth.instance
            .signInWithEmailAndPassword(email: email, password: password);
        return userCredential;
      } on FirebaseAuthException catch (e) {
        if (e.code == 'user-not-found') {
          AwesomeDialog(
              context: context,
              title: "Error",
              body: Text('No user found for that email.'))
            ..show();
        } else if (e.code == 'wrong-password') {
          AwesomeDialog(
              context: context,
              title: "Error",
              body: Text('Wrong password provided for that user.'))
            ..show();
        }
      }
    } else {
      print("not valid");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: (Color(0x0ff79A0C9)),
      appBar: AppBar(
        title: Text('Log In'),
        backgroundColor: (Color(0x0ff4676B4)),
      ),
      body: ListView(

        children: [
          Center(
            child: myImage(),
          ),
          Container(
              padding: EdgeInsets.only(right: 40, left: 40, top: 0, bottom: 10),
              child: Form(
                  key: formstate, //59
                  child: Column(children: [
                    SizedBox(
                      height: 10,
                    ),
                    Center(
                        child: Text(
                      'Log In ',
                      style:
                          TextStyle(color: (Color(0x0ffF6F7F9)), fontSize: 20),
                    )),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      onSaved: (val) {
                        email = val;
                      },
                      validator: (val) {
                        if (val.length > 25) {
                          return "Email can't to be larger than 15 letter";
                        }
                        if (val.length < 3) {
                          return "Email can't to be less than 3 letter";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.email,
                          color: Colors.white,
                        ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: 'Email',
                        hintText: 'ex: JackPot@gmail.com',
                        labelStyle: TextStyle(color: Colors.blueGrey),
                        errorStyle: TextStyle(color: Colors.red),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      onSaved: (val) {
                        password = val;
                      },
                      validator: (val) {
                        if (val.length > 15) {
                          return "password can't to be larger than 20 letter";
                        }
                        if (val.length < 4) {
                          return "password can't to be less than 3 letter";
                        }
                        return null;
                      },

                      obscureText: _secureText,
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.lock,
                          color: Colors.white,
                        ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: 'Password',
                        labelStyle: TextStyle(color: Colors.blueGrey),
                        errorStyle: TextStyle(color: Colors.red),
                        suffixIcon: IconButton(
                         icon:  Icon(Icons.remove_red_eye),
                          onPressed: (){
                           setState(() {
                             _secureText = !_secureText;
                           });
                          },
                        )
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Checkbox(
                            value: remember,
                            onChanged: (email) {
                              setState(() {
                                remember = email;
                              });
                            }),
                        Text("Remember me",
                            style: TextStyle(color: Colors.white))
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    RaisedButton(
                      onPressed: () async {
                        var user = await logIn();
                        if (user != null &&
                            FirebaseAuth.instance.currentUser.emailVerified ==
                                true) {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      mainPage()));
                        } else {
                          AwesomeDialog(
                              context: context,
                              title: "Error",
                              body: Text("Please Verify Your Account"))
                            ..show();
                        }
                      },
                      child: Text(
                        'Log In',
                        style: TextStyle(
                            color: (Color(0x0ffF6F7F9)), fontSize: 30),
                      ),
                      color: (Color(0x0ff4676B4)),
                      elevation: 50,
                      padding: EdgeInsets.only(
                          right: 90, left: 90, top: 5, bottom: 5),
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0)),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Divider(
                      height: 7,
                      color: Colors.blueAccent,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    InkWell(
                      child: Center(
                          child: Text('create new account',
                              style: TextStyle(
                                  color: (Color(0x0ffD3DDE9)), fontSize: 20))),
                      onTap: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) => signUp()));
                      },
                    ),
                    SizedBox(height: 20),
                    InkWell(
                      child: Center(
                          child: Text('forget password ?',
                              style: TextStyle(
                                  color: (Color(0x0ffD3DDE9)),
                                  fontSize: 15,
                                  decoration: TextDecoration.underline))),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ForgetPassword(),
                            ));
                      },
                    )
                  ])))
        ],
      ),
    );
  }
}
//JackPot logo Image
class myImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AssetImage image = new AssetImage('images/jp.png');
    Image myImg = new Image(image: image);
    return Container(
      child: myImg,
      width: 800,
    );
  }
}
