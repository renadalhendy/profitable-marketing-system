import 'package:flutter/material.dart';
import 'main.dart';
import 'mainpage.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Surveys extends StatefulWidget {
  @override
  _Surveys createState() => _Surveys();
}
//Surveys List .. Surveys Will Be Generated Automatically
class _Surveys extends State<Surveys> {
  final Stream<QuerySnapshot> _usersStream =
      FirebaseFirestore.instance.collection('surveys').snapshots();
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _usersStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }
        //Waiting For Fetching Data
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }
        return Scaffold(
            backgroundColor: (Color(0x0ff79A0C9)),
            appBar: AppBar(
              title: Text('Surveys'),
              backgroundColor: (Color(0x0ff4676B4)),
            ),
            body: new ListView(
              children: snapshot.data.docs.map((DocumentSnapshot document) {
                Map<String, dynamic> data =
                    document.data() as Map<String, dynamic>;
                return Material(
                  // Survey Details
                  child: new ListTile(
                    leading: Icon(Icons.launch),
                    title: new Text("${data['points']}  points"),
                    subtitle: new Text("${data['time']}  min"),
                    trailing: Icon(Icons.arrow_forward_outlined),
                    onTap: () {
                      launch(data['url']);
                    },
                  ),
                );
              }).toList(),
            ));
      },
    );
  }
}
