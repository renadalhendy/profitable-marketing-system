import 'package:flutter/material.dart';
import 'mainpage.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Offers extends StatefulWidget {
  @override
  _Offers createState() => _Offers();
}
//offers page .. offfers will be generated automatically
class _Offers extends State<Offers> {
  final Stream<QuerySnapshot> _usersStream =
      FirebaseFirestore.instance.collection('offers').snapshots();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _usersStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }

        return Scaffold(
            backgroundColor: (Color(0x0ff79A0C9)),
            appBar: AppBar(
              title: Text('Offers'),
              backgroundColor: (Color(0x0ff4676B4)),
            ),
            body: new ListView(
              children: snapshot.data.docs.map((DocumentSnapshot document) {
                Map<String, dynamic> data =
                    document.data() as Map<String, dynamic>;
                return Material(
                  child: new ListTile(
                    leading: Icon(Icons.launch),
                    title: new Text("${data['points']}  points"),
                    subtitle: new Text(data['task']),
                    trailing: Icon(Icons.arrow_forward_outlined),
                    onTap: () {
                      launch(data['url']);
                    },
                  ),
                );
              }).toList(),
            ));
      },
    );
  }
}
