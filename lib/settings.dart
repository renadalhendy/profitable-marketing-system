import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'main.dart';
import 'package:flutter/material.dart';
import 'login.dart';

var us;
var points;
///// get user info///
getUsername() async {
  var user = await FirebaseAuth.instance.currentUser;
  FirebaseFirestore.instance
      .collection("users")
      .where("email", isEqualTo: user.email)
      .get()
      .then((value) {
    value.docs.forEach((element) {
      us = element.data()["username"];
      points = element.data()["points"];

    });
  });
}

class Settingss extends StatefulWidget {
  Settingss({Key key}) : super(key: key);

  @override
  _Settings createState() => _Settings();
}

class _Settings extends State<Settingss> {
  @override
  Widget build(BuildContext context) {
    getUsername();
    return Scaffold(
      backgroundColor: (Color(0x0ff79A0C9)),
      appBar: AppBar(
        title: Text('Settings'),
        backgroundColor: (Color(0x0ff4676B4)),
      ),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.only(right: 40, left: 40, top: 0, bottom: 10),
            child: Form(
              child: Column(
                children: [
                  SizedBox(height: 30),
                  Center(
                    child: Text("Your Information",
                        style: TextStyle(
                            color: (Color(0x0ffF6F7F9)), fontSize: 20)),
                  ),
                  SizedBox(height: 30),
                  Row(
                    children: [
                      Text(
                        "Username: ",
                        style: TextStyle(
                            color: (Color(0x0ffF6F7F9)), fontSize: 20),
                      ),
                      Text(
                        "$us",
                        style:
                        TextStyle(color: Colors.lightGreen, fontSize: 20),
                      ),
                    ],
                  ),
                  Divider(height: 10, thickness: 3),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Text(
                        "Email:",
                        style: TextStyle(
                            color: (Color(0x0ffF6F7F9)), fontSize: 20),
                      ),
                      Text(
                        FirebaseAuth.instance.currentUser.email,
                        style:
                        TextStyle(color: Colors.lightGreen, fontSize: 20),
                      )
                    ],
                  ),
                  Divider(height: 10, thickness: 3),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
