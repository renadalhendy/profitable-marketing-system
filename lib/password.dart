import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:test_login1/login.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:test_login1/mainpage.dart';

String _email;

class ForgetPassword extends StatefulWidget {
  @override
  _ForgetPassword createState() => _ForgetPassword();
}

class _ForgetPassword extends State<ForgetPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: (Color(0x0ff79A0C9)),
        appBar: AppBar(
          title: Text('Log In'),
          backgroundColor: (Color(0x0ff4676B4)),
        ),
        body: ListView(
          padding: EdgeInsets.only(right: 40, left: 40, top: 60, bottom: 60),
          children: [
            Center(
              child: Text("Forget Password",
                  style: TextStyle(fontSize: 25, color: (Color(0x0ffF6F7F9)))),
            ),
            SizedBox(height: 10),
            Center(
              child: Text(
                  "please enter your email and we will send you a link to reset your password within 2 days",
                  style: TextStyle(fontSize: 15, color: (Color(0x0ffF6F7F9)))),
            ),
            SizedBox(height: 60),
            TextFormField(
              decoration: InputDecoration(
                prefixIcon: Icon(
                  Icons.perm_identity_rounded,
                  color: Colors.white,
                ),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                labelText: 'Enter Your Email',
                hintText: 'ex: JackPot@gmail.com',
                labelStyle: TextStyle(color: Colors.blueGrey),
                errorStyle: TextStyle(color: Colors.red),
              ),
              onChanged: (value) {
                setState(() {
                  _email = value;
                });
              },
            ),
            SizedBox(height: 40),
            RaisedButton(
              onPressed: () {
                FirebaseAuth.instance.sendPasswordResetEmail(email: _email);
                AwesomeDialog(
                    context: context,
                    title: "Alert",
                    body: Text("We Send You An Email To Reset Your Password"))
                  ..show();
                Navigator.of(context).pop();
              },
              child: Text("Send Request",
                  style: TextStyle(color: (Color(0x0ffF6F7F9)), fontSize: 30)),
              color: (Color(0x0ff4676B4)),
              elevation: 50,
            )
          ],
        ));
  }
}
